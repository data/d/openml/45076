# OpenML dataset: churn

https://www.openml.org/d/45076

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

A dataset relating characteristics of telephony account features and usage and whether or not the customer churned. Originally used in Discovering Knowledge in Data: An Introduction to Data Mining.Source: https://www.secs.ac.in/wp-content/CSE_PORTAL/DataMining_Daniel.pdf

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45076) of an [OpenML dataset](https://www.openml.org/d/45076). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45076/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45076/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45076/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

